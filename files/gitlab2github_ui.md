# Usando la interfaz de usuario

**1)** vamos a importar el siguiente repositorio de gitlab a github : https://gitlab.com/gold-saints/appointment.git

**3)** ingresamos a nuestra cuenta de github y hacemos clic en "+ > import repository" parar importar un repositorio.

![image info](../img/ui01.jpg)

**4)** ingresamos los datos requeridos, utilizamos el mismo nombre del repositorio fuente.

![image info](../img/ui02.jpg)

**5)** ingresamos nuestras credenciales de la plataforma del repositorio fuente en caso sean requeridas.

![image info](../img/ui03.jpg)

**6)** esperamos hasta que culmine la importación.

![image info](../img/ui04.jpg)

**7)** si la rama *master* no esta establecida por defecto, entonces seguimos los siguientes pasos :

![image info](../img/general03.jpg)
![image info](../img/general04.jpg)
![image info](../img/general05.jpg)
![image info](../img/general06.jpg)

**8)** ingresamos al repositorio https://github.com/Coracaos/appointment.git de github para verificar que *master* sea la rama por defecto (a veces se tarda unos minutos en aplicar los cambios, esperar un momento).