# Usando comandos de git

**1)** vamos a importar el siguiente repositorio de gitlab a github : https://gitlab.com/gold-saints/appointment

**2)** creamos un repositorio en github con el mismo nombre del repositorio de github.

![image info](../img/general01.jpg)
![image info](../img/general02.jpg)

**3)** ejecutamos los siguientes comandos para importar el repositorio de gitlab a github :

```bash
$ git clone --mirror https://gitlab.com/gold-saints/appointment.git
$ cd appointment.git
$ git push --mirror https://github.com/Coracaos/appointment.git
```

**4)** ingresamos al repositorio https://github.com/Coracaos/appointment.git de github para verificar.

**5)** si la rama *master* no esta establecida por defecto, entonces seguimos los siguientes pasos :

![image info](../img/general03.jpg)
![image info](../img/general04.jpg)
![image info](../img/general05.jpg)
![image info](../img/general06.jpg)

**6)** ingresamos al repositorio https://github.com/Coracaos/appointment.git de github para verificar que *master* sea la rama por defecto. (a veces se tarda unos minutos en aplicar los cambios, esperar un momento).