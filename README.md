# Gitlab to Github
Se presentan dos formas para importar un repositorio de gitlab a github :

- [Usando comandos](./files/gitlab2github_command.md)
- [Usando la interfaz de usuario](./files/gitlab2github_ui.md)